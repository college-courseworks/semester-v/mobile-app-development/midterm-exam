package com.example.midtermexam

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Disable dark theme
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        // Spinner
        val districts = resources.getStringArray(R.array.districts)
        val spinner = findViewById<Spinner>(R.id.spinner)

        var selectedDistrict: String? = null

        if (spinner != null) {
            val adapter = ArrayAdapter(this, R.layout.custom_spinner_item, districts)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter

            // Get selected spinner item
            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    selectedDistrict = parent?.getItemAtPosition(position) as String
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }
        }
        6
        // Text input
        val textInputLayout = findViewById<TextInputLayout>(R.id.text_input_layout)
        val textInputEditText = findViewById<TextInputEditText>(R.id.text_input_edit_text)

        val originalLabel = textInputLayout.hint

        // Change input layout hint
        textInputEditText.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                textInputLayout.hint = null
            } else {
                textInputLayout.hint = originalLabel
            }
        }

        // Submit
        val submitButton = findViewById<Button>(R.id.submit_button)

        submitButton.setOnClickListener {
            val name: String = textInputEditText.text.toString().trim()

            if (name.isEmpty()) {
                Toast.makeText(this, "Please fill name field", Toast.LENGTH_SHORT).show()
            } else {
                val district: String = selectedDistrict ?: "Cibiru"

                Intent(this, SecondActivity::class.java).also {
                    it.putExtra("EXTRA_NAME", name)
                    it.putExtra("EXTRA_DISTRICT", district)
                    startActivity(it)
                }
            }
        }
    }
}
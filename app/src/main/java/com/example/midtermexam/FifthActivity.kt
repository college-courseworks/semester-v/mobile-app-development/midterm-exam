package com.example.midtermexam

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast

class FifthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fifth)

        val orderName = findViewById<TextView>(R.id.order_name)
        val orderDistrict = findViewById<TextView>(R.id.order_district)
        val radioGroup = findViewById<RadioGroup>(R.id.radio_group)
        val radioTake = findViewById<RadioButton>(R.id.radio_take)
        val doneButton = findViewById<Button>(R.id.done_button)

        val extraName = intent.getStringExtra("EXTRA_NAME")
        val extraDistrict = "Store: " + intent.getStringExtra("EXTRA_DISTRICT")

        orderName.text = extraName
        orderDistrict.text = extraDistrict

        doneButton.setOnClickListener {
            val selectedRadioButton = radioGroup.checkedRadioButtonId
            var message = "Please select delivery options"

            if (selectedRadioButton != -1) {

                message = if (selectedRadioButton == radioTake.id) {
                    "Terimakasih $extraName sudah memesan di Toko kami, silahkan ambil pesanan di Toko $extraDistrict"
                } else {
                    "Terimakasih $extraName sudah memesan di Toko kami, pesanan anda akan dikirim menggunakan Fast Delivery"
                }
            }

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }
    }
}
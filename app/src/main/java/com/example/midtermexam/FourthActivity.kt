package com.example.midtermexam

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class FourthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fourth)

        val orderButton = findViewById<Button>(R.id.order_button)
        val backButton = findViewById<Button>(R.id.back_button)

        val extraName = intent.getStringExtra("EXTRA_NAME")
        val extraDistrict = intent.getStringExtra("EXTRA_DISTRICT")

        orderButton.setOnClickListener {
            Intent(this, FifthActivity::class.java).also {
                it.putExtra("EXTRA_NAME", extraName)
                it.putExtra("EXTRA_DISTRICT", extraDistrict)
                startActivity(it)
            }
        }

        backButton.setOnClickListener {
            finish()
        }
    }
}
package com.example.midtermexam

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val textName = findViewById<TextView>(R.id.user_name)
        val textDistrict = findViewById<TextView>(R.id.store_district)
        val seeMenusButton = findViewById<Button>(R.id.menus_button)

        val extraName = intent.getStringExtra("EXTRA_NAME")
        val extraDistrict = intent.getStringExtra("EXTRA_DISTRICT")
        val userMessage = "Hello, $extraName"

        textName.text = userMessage
        textDistrict.text = extraDistrict

        seeMenusButton.setOnClickListener {
            Intent(this, ThirdActivity::class.java).also {
                it.putExtra("EXTRA_NAME", extraName)
                it.putExtra("EXTRA_DISTRICT", extraDistrict)
                startActivity(it)
            }
        }
    }
}
package com.example.midtermexam

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ThirdActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)

        val textName = findViewById<TextView>(R.id.user_name)
        val addButton = findViewById<FloatingActionButton>(R.id.fab_add)

        val extraName = intent.getStringExtra("EXTRA_NAME")
        val extraDistrict = intent.getStringExtra("EXTRA_DISTRICT")
        val userMessage = "Hello, $extraName"

        textName.text = userMessage

        addButton.setOnClickListener {
            Intent(this, FourthActivity::class.java).also {
                it.putExtra("EXTRA_NAME", extraName)
                it.putExtra("EXTRA_DISTRICT", extraDistrict)
                startActivity(it)
            }
        }
    }
}